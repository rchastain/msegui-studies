
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  msetypes,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  msewidgets,
  mseforms,
  mseimage,
  msebitmap,
  mseformatbmpicoread,
  mseformatjpgread,
  mseformatpngread,
  mseformatpnmread,
  mseformattgaread,
  mseformatxpmread,
  msekeyboard,
  mseact,
  msedataedits,
  mseedit,
  msefiledialog,
  msestatfile,
  msestream,
  msesys,
  msesimplewidgets,
  msefileutils,
  msetoolbar,
  classes,
  sysutils;

type
  tmainfo = class(tmainform)
    FImage: timage;
    FEditDirectory: tfilenameedit;
    FLabelFilesCount: tlabel;
    FButtonLeft: tbutton;
    FButtonRight: tbutton;
    FButtonQuit: tbutton;
    FIcons: timagelist;
    FToolBar: ttoolbar;
    FEditFileName: tstringedit;
    procedure FormCreate(const sender: TObject);
    procedure FormDestroy(const sender: TObject);
    procedure FormKeyDown(const sender: twidget; var ainfo: keyeventinfoty);
    procedure FileNameEditChange(const sender: TObject);
    procedure ButtonQuitExecute(const sender: TObject);
    procedure ButtonLeftExecute(const sender: TObject);
    procedure ButtonRightExecute(const sender: TObject);
    procedure FormLoaded(const sender: TObject);
    procedure ItemRenameExecute(const sender: TObject);
    procedure ItemDeleteExecute(const sender: TObject);
    procedure ItemAboutExecute(const sender: TObject);
  protected
    FLoader: TMaskedBitmap;
    FList: TStringList;
    FImageIndex: integer;
    procedure LoadImage(const AFileName: string);
    procedure UpdateLabel(const AImageIndex, AFilesCount: integer);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm,
  math,
  findfiles,
  log,
  translationmanager; (* https://www.lazarusforum.de/viewtopic.php?t=11928 *)

const
  CAmerican = 'en-us';
  CEnglish  = 'en-gb';
  CFrench   = 'fr-fr';
  CGerman   = 'de-de';
  
  CDefaultLanguage = CFrench; // <---
  
  fn: filenamety = 'imageviewer.sta';
  
var
  LLastDir: filenamety;
  LLanguage: string;
    
procedure tmainfo.FormCreate(const sender: TObject);
var
  sr: tstatreader;
begin
  LogLn('FormCreate');
  try
    sr := tstatreader.create(fn);
    with sr do
    begin
      setsection('options');
      LLanguage := string(readmsestring('lang', CDefaultLanguage));
      setsection('history');
      LLastDir := readmsestring('directory', '');
     {WriteLn(readboolean('b', FALSE));
      WriteLn(readinteger('c', 0));}
    end;
  finally
    sr.free;
  end;
  
  FLoader := TMaskedBitmap.Create(BMK_RGB);
  FList := TStringList.Create;
  FList.Sorted := TRUE;
  FImageIndex := -1;

  TransMgr.IniFile := 'lang.cfg';
  TransMgr.Language := LLanguage;
  LogLn(Format('TransMgr.LanguageName=%s', [TransMgr.LanguageName]));
  Caption := UTF8Decode(TransMgr.GetString('appname'));
  FButtonQuit.Caption := UTF8Decode(TransMgr.GetString('exit'));
  FEditDirectory.Hint := UTF8Decode(TransMgr.GetString('current'));
  FToolbar.Buttons[0].Hint := UTF8Decode(TransMgr.GetString('rename'));
  FToolbar.Buttons[1].Hint := UTF8Decode(TransMgr.GetString('delete'));
  FToolbar.Buttons[2].Hint := UTF8Decode(TransMgr.GetString('about'));
  FToolbar.Buttons[3].Hint := UTF8Decode(TransMgr.GetString('quit'));
  FButtonLeft.Hint := UTF8Decode(TransMgr.GetString('previous'));
  FButtonRight.Hint := UTF8Decode(TransMgr.GetString('next'));
  
  //FToolbar.Buttons[0].Enabled := FALSE;
end;

procedure tmainfo.FormDestroy(const sender: TObject);
var
  sw: tstatwriter;
begin
  try
    sw := tstatwriter.create(fn);
    with sw do
    begin
      writesection('options');
      writemsestring('lang', utf8tostring(LLanguage));
      writesection('history');
      writemsestring('directory', LLastDir);
     {writeboolean('b', TRUE);
      writeinteger('c', 2020);}
    end;
  finally
    sw.free;
  end;
  FLoader.Destroy;
  FList.Free;
end;

procedure tmainfo.FormKeyDown(const sender: twidget; var ainfo: keyeventinfoty);
begin
  case ainfo.key of
    KEY_LEFT: ButtonLeftExecute(sender);
    KEY_RIGHT: ButtonRightExecute(sender);
    KEY_ESCAPE: Close;
  end;
end;

procedure tmainfo.LoadImage(const AFileName: string);
var
  LSize: sizety;
  LXRatio, LYRatio, LRatio: double;
begin
  LogLn(Format('LoadImage(%s)', [AFileName]));
  
  application.beginwait();
  
  FLoader.LoadFromFile(UTF8Decode(AFileName));
  LSize := FLoader.Size;
  LXRatio := FImage.Size.cx / LSize.cx;
  LYRatio := FImage.Size.cy / LSize.cy;
  if (LXRatio < 1) or (LYRatio < 1) then
  begin
    LRatio := Min(LXRatio, LYRatio);
    LSize.cx := Round(LRatio * LSize.cx);
    LSize.cy := Round(LRatio * LSize.cy);
  end;
  FImage.Bitmap.Size := LSize;
  FLoader.Stretch(FImage.Bitmap);
  FEditFileName.Value := UTF8Decode(ExtractFileName(AFileName));
  
  application.endwait();
end;

procedure tmainfo.UpdateLabel(const AImageIndex, AFilesCount: integer);
begin
  if AImageIndex > -1 then
    FLabelFilesCount.Caption := UTF8Decode(TransMgr.GetString('image') + ' ' + IntToStr(Succ(AImageIndex)) + ' / ' + IntToStr(AFilesCount))
  else
    FLabelFilesCount.Caption := '';
end;

procedure tmainfo.FileNameEditChange(const sender: TObject);
const
  CFileMask = '^(.+\.bmp|.+\.ico|.+\.jpeg|.+\.jpg|.+\.png|.+\.xpm)$';
var
  LSize: sizety;
begin
  LogLn('FileNameEditChange');
  FList.Clear;
  SearchFiles(FList, string({$IFDEF mswindows}tosysfilepath(FEditDirectory.Value){$ELSE}FEditDirectory.Value{$ENDIF}), CFileMask, FALSE);
  
  if FList.Count > 0 then
  begin
    FImageIndex := 0;
    LoadImage(FList[FImageIndex]);
  end else
  begin
    FImageIndex := -1;
    LSize.cx := 0;
    LSize.cy := 0;
    FImage.Bitmap.Size := LSize;
    FEditFileName.Value := '';
  end;
  
  UpdateLabel(FImageIndex, FList.Count);
  
  LLastDir := FEditDirectory.Value;
end;

procedure tmainfo.ButtonQuitExecute(const sender: TObject);
begin
  Close;
end;

procedure tmainfo.ButtonLeftExecute(const sender: TObject);
begin
  Dec(FImageIndex);
  if FImageIndex < 0 then
    FImageIndex := Pred(FList.Count);
  LoadImage(FList[FImageIndex]);
  UpdateLabel(FImageIndex, FList.Count);
end;

procedure tmainfo.ButtonRightExecute(const sender: TObject);
begin
  Inc(FImageIndex);
  if FImageIndex > Pred(FList.Count) then
    FImageIndex := 0;
  LoadImage(FList[FImageIndex]);
  UpdateLabel(FImageIndex, FList.Count);
end;

procedure tmainfo.FormLoaded(const sender: TObject);
begin
  LogLn('FormLoaded');
  if (ParamCount = 1) and DirectoryExists(ParamStr(1)) then
    FEditDirectory.Value := UTF8Decode(ParamStr(1))
  else if (ParamCount = 1) and DirectoryExists(ExtractFileDir(ParamStr(1))) then
    FEditDirectory.Value := UTF8Decode(ExtractFileDir(ParamStr(1)))
  else if (Length(LLastDir) > 0) and DirectoryExists(LLastDir) then
    FEditDirectory.Value := LLastDir
  else
    FEditDirectory.Value := UTF8Decode(ExtractFileDir(ParamStr(0)));
end;

procedure tmainfo.ItemRenameExecute(const sender: TObject);
var
  LNewName: TFileName;
begin
  LNewName := ExtractFilePath(FList[FImageIndex]) + string(FEditFileName.Value); (* L'image reste toujours dans le même dossier. *)
  if FileExists(LNewName) then
  begin
    ShowMessage(UTF8Decode(TransMgr.GetString('exists')));
    Exit;
  end;
  LogLn(Format('RenameFile(%s,%s)', [FList[FImageIndex], LNewName]));
  RenameFile(FList[FImageIndex], LNewName);
  FList.Sorted := FALSE;
  FList[FImageIndex] := LNewName;
  FList.Sorted := TRUE;
end;

procedure tmainfo.ItemDeleteExecute(const sender: TObject);
var
  LSize: sizety;
begin
  LogLn(Format('DeleteFile(%s)', [FList[FImageIndex]]));
  DeleteFile(FList[FImageIndex]);
  FList.Delete(FImageIndex);
  
  if FList.Count > 0 then
    ButtonLeftExecute(sender)
  else
  begin
    FImageIndex := -1;
    LSize.cx := 0;
    LSize.cy := 0;
    FImage.Bitmap.Size := LSize;
    FEditFileName.Value := '';
  end;
  
  UpdateLabel(FImageIndex, FList.Count);
end;

procedure tmainfo.ItemAboutExecute(const sender: TObject);
begin
  ShowMessage(UTF8Decode(TransMgr.GetString('appname')  + ' ' + {$I version}));
end;

end.
