
## Create desktop launcher and shell script for Image Viewer

## Executable name
EXE="imageview"
## Desktop file name
DF="$EXE.desktop"
## Application name
APP="MSEgui Image Viewer 0.3"
## Script directory
SCRDIR="$(dirname "$(readlink -f "$0")")"
## Desktop directory
DDIR="$HOME/Desktop"

if [ ! -d $DDIR ] ;
then
  echo "Cannot find directory $DDIR"
  DDIR="$HOME/Bureau"
fi

## Create desktop launcher

if [ -d $DDIR ] ;
then
  echo "Found directory $DDIR"

  FILE=$DDIR/$DF

  echo "Creating desktop launcher $FILE"

  cat > $FILE << EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=$APP
Comment=
Exec=$SCRDIR/$EXE %F
Icon=$SCRDIR/icon.png
Path=$SCRDIR
Terminal=false
StartupNotify=true
EOF

  echo "Making launcher executable"

  sudo chmod -R 777 $FILE

  echo "Done"

else
  echo "Cannot find directory $DDIR"
fi

## Create shell script

FILE=$EXE.sh

echo "Creating script $FILE"

cat > $FILE << EOF
APPDIR=$SCRDIR
\$APPDIR/$EXE \$*
EOF

echo "Making script executable"

sudo chmod -R 777 $FILE

echo "Done"
