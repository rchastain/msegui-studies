set UNITS=%~dp0units

if exist %UNITS% (
del /s /q %UNITS%/*.*
) else (
mkdir %UNITS%
)

set MSEDIR=%~d0\applications\mseide\463-240202

REM set "OPTIONS=-Xg -gl -O-"
set "OPTIONS=-O2 -XX -Xs -CX -B"

echo "Compiling project"

fpc ^
-Fu%MSEDIR%\lib/common/* ^
-Fu%MSEDIR%\lib/common/kernel ^
-Fi%MSEDIR%\lib/common/kernel ^
-Fu%MSEDIR%\lib/common/kernel/windows ^
-Fu%MSEDIR%\lib/addon/* ^
-Fi%MSEDIR%\lib/addon/* ^
-FU%UNITS%/ ^
-l -Mobjfpc -Sh %OPTIONS% imageview.pas ^
> build.log 

:: -Fcutf8
