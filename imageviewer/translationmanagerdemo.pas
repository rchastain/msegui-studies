
program TranslationManagerDemo;

uses
  SysUtils,
  TranslationManager; (* https://www.lazarusforum.de/viewtopic.php?t=11928 *)

const
  CAmerican = 'en-us';
  CEnglish  = 'en-gb';
  CFrench   = 'fr-fr';
  CGerman   = 'de-de';
  
  CDefaultLanguage = CFrench; // <---

var
  LFile: textfile;
  s: string;
  
begin
  WriteLn('DefaultSystemCodePage = ', DefaultSystemCodePage);
  
  TransMgr.IniFile := 'lang.cfg';
  TransMgr.Language := CDefaultLanguage;
  
  s := TransMgr.GetString('about');
  
  WriteLn('StringCodepage(s) = ', StringCodepage(s));
  
  Assign(LFile, 'translationmanagerdemo.log');
  Rewrite(LFile);
  WriteLn(LFile, s);
  Close(LFile);
end.
