# Visionneuse d'images MSEgui

Simple application permettant 1° de **visionner** les images contenues dans un dossier, et 2° de les **renommer** ou de les **supprimer** rapidement.

Application multiplateforme basée sur la bibliothèque [MSEgui](https://github.com/mse-org/mseide-msegui).

## Capture d'écran

![Screenshot](screenshot.png)

## Mode d'emploi

Au démarrage, l'application cherche les images contenues dans le même dossier que l'exécutable, à moins que le chemin d'un dossier ou d'un fichier ne soit passé en paramètre. L'application affiche la première image de la liste et le nombre d'images trouvées.

Pour choisir un autre dossier, cliquez sur le bouton *Naviguer*.

Pour renommer un fichier, changez le nom dans la zone d'édition puis cliquez sur le bouton *Renommer* (premier bouton de la barre d'outils). L'image reste toujours dans le même dossier.

Pour supprimer un fichier, cliquez sur le bouton *Supprimer* (deuxième bouton de la barre d'outils).

## Ressources utilisées

Les icônes proviennent de la collection [Silk Icons](https://github.com/legacy-icons/famfamfam-silk).

La traduction de l'interface est assurée par l'unité [LightWeightTranslationManager](https://www.lazarusforum.de/viewtopic.php?t=11928).
