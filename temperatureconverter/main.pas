unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseforms,
  msedataedits,
  sysutils;

type
  tmainfo = class(tmainform)
    EDCelsius: trealedit;
    EDFahrenheit: trealedit;
    procedure OnCelsiusDataEntered(const sender: TObject);
    procedure OnFahrenheitDataEntered(const sender: TObject);
  end;
  
var
  mainfo: tmainfo;
  
implementation

uses
  main_mfm,
  meteoconv;

function OneDecimalValue(const AValue: double): double;
begin
  result := Round(10 * AValue) / 10;
end;

procedure tmainfo.OnCelsiusDataEntered(const sender: TObject);
begin
  EDFahrenheit.value := OneDecimalValue(CentToFahr(EDCelsius.value));
end;

procedure tmainfo.OnFahrenheitDataEntered(const sender: TObject);
begin
  EDCelsius.value := OneDecimalValue(FahrToCent(EDFahrenheit.value));
end;

end.
