unit meteoconv;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

function FahrToCent(Fahr: double): double;
function CentToFahr(Cent: double): double;
function CentToKelvin(Cent: double): double;
function KelvinToCent(Kelvin: double): double;
function KelvinToFahr(Kelvin: double): double;
function RankineToCent(Rankine: double): double;
function CentToRankine(Celsius: double): double;
function RankineToFahr(Rankine: double): double;
function ReaumurToCent(Reaumur: double): double;
function NewtonToCent(Newton: double): double;

implementation

(* https://www.gladir.com/CODER/TPASCAL7/meteoconv.htm *)

function FahrToCent(Fahr: double): double;
begin
  result := (5.0 / 9.0) * (Fahr - 32.0);
end;

function CentToFahr(Cent: double): double;
begin
  result := 1.8 * Cent + 32.0;
end;

function CentToKelvin(Cent: double): double;
begin
  result := Cent + 273.16;
end;

function KelvinToCent(Kelvin: double): double;
begin
  result := Kelvin - 273.16;
end;

function KelvinToFahr(Kelvin: double): double;
begin
  result := 1.8 * (Kelvin - 273.16) + 32.0;
end;

function RankineToCent(Rankine: double): double;
begin
  result := (5.0 / 9.0) * (Rankine - 491.69);
end;

function CentToRankine(Celsius: double): double;
begin
  result := (Celsius * 1.8) + 491.69;
end;

function RankineToFahr(Rankine: double): double;
begin
  result := Rankine - 459.69;
end;

function ReaumurToCent(Reaumur: double): double;
begin
  result := Reaumur * 5 / 4;
end;

function NewtonToCent(Newton: double): double;
begin
  result := Newton * 100 / 33;
end;

end.
