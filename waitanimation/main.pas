
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  msetypes,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  msewidgets,
  mseforms,
  msesimplewidgets,
  msedispwidgets,
  mserichstring,
  msetimer,
  msethread,
  sysutils,
  math;

type
  ttestthreadcontainer = class
  private
    FThread: tmsethread;
  protected
    function Execute(thread: tmsethread): integer;
  public
    destructor Destroy; override;
    procedure Run;
  end;

  tmainfo = class(tmainform)
    btExecute: tbutton;
    tmWait: ttimer;
    pbWait: tpaintbox;
    procedure ButtonExecute(const sender: TObject);
    procedure FormCreate(const sender: TObject);
    procedure TimerExecute(const sender: TObject);
    procedure PaintBoxPaint(const sender: twidget; const acanvas: tcanvas);
    procedure FormDestroy(const sender: TObject);
  private
    FTicks: cardinal;
    FThreadContainer: ttestthreadcontainer;
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm;

{ ttestthreadcontainer }

destructor ttestthreadcontainer.Destroy;
begin
  if FThread <> nil then
  begin
    FThread.Terminate;
    application.WaitForThread(FThread);
    FThread.Destroy;
  end;
  inherited;
end;

procedure ttestthreadcontainer.Run;
begin
  FThread := tmsethread.Create(@Execute);
end;

function ttestthreadcontainer.Execute(thread: tmsethread): integer;
begin
  try
    //application.Lock;
    Sleep(10000);
    mainfo.pbWait.Visible := FALSE;
    mainfo.tmWait.Enabled := FALSE;
  finally
    //application.Unlock;
  end;
  result := 0;
end;

{ tmainfo }

procedure tmainfo.ButtonExecute(const sender: TObject);
begin
  tmWait.Enabled := TRUE;
  pbWait.Visible := TRUE;
  FThreadContainer.Run;
end;

procedure tmainfo.FormCreate(const sender: TObject);
begin
  FThreadContainer := ttestthreadcontainer.Create();
end;

procedure tmainfo.TimerExecute(const sender: TObject);
begin
  pbWait.Invalidate;
end;

procedure tmainfo.PaintBoxPaint(const sender: twidget; const acanvas: tcanvas);
(* http://www.developpez.net/forums/d1533745/environnements-developpement/delphi/contribuez/animation-patienter/ *)
var
  t: cardinal;
  d: cardinal;
  i: integer;
  x, y: integer;
  w: integer;
  cs, sn: extended;
  c: integer;
begin
  t := GetTickCount64;

  if FTicks = 0 then
  begin
    d := 0;
    FTicks := t;
  end else
  begin
    d := (t - FTicks) mod 4000;
  end;

  x := sender.bounds_cx div 2;
  y := sender.bounds_cy div 2;
  w := 2 * x div 3;
  for i := 0 to 7 do
  begin
    c := 255 - 64 + Round(32 * Cos(PI * i / 4));
    SinCos(PI * ((1 + i / 4) * Cos(PI * d / 4000)), cs, sn);
    acanvas.FillRect(
      MakeRect(x + Round(w * cs) - 2, y + Round(w * sn) - 2, 4, 4),
      //RGBToColor(c, c, c)
      //CL_BLUE
      BlendColor(c / 255, sender.color, CL_BLUE)
      );
  end;
end;

procedure tmainfo.FormDestroy(const sender: TObject);
begin
  FThreadContainer.Free;
end;

end.
