
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob, mseguiglob, mseguiintf, mseapplication, msestat, msemenus, msegui,
  msegraphics, msegraphutils, mseevent, mseclasses, mseforms, msesimplewidgets,
  msewidgets, msetimer,
  math;

type
  tmainfo = class(tmainform)
    pbBox: tpaintbox;
    btStart: tbutton;
    tmTimer: ttimer;
    procedure PaintBoxPaint(const sender: twidget; const acanvas: tcanvas);
    procedure FormCreate(const sender: TObject);
    procedure TimerExecute(const sender: TObject);
    procedure ButtonExecute(const sender: TObject);
  private
    FAngle: integer;
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm;

const
  CGrandRayon = 150;
  CPetitRayon = 120;
  CDistanceFoyer = 90;

(* 90² = 150² - 120² *)

procedure tmainfo.PaintBoxPaint(const sender: twidget; const acanvas: tcanvas);
var
  s, c: double;
  x, y: integer;
begin
  (* Repère *)
  acanvas.DrawLine(MakePoint(0, 200), MakePoint(400, 200), CL_DKBLUE);
  acanvas.DrawLine(MakePoint(200, 0), MakePoint(200, 400), CL_DKBLUE);

  SinCos(DegToRad(FAngle), s, c);
  x := Round(CGrandRayon * c + 200);
  y := Round(CPetitRayon * s + 200);

  (* Soleil *)
  acanvas.FillCircle(MakePoint(200 - CDistanceFoyer, 200), 30, CL_YELLOW, CL_YELLOW);
  (* Terre *)
  acanvas.FillCircle(MakePoint(x, y), 10, CL_BLUE, CL_BLUE);
  (* Lignes reliant les foyers de l'ellipse au centre de la terre *)
  acanvas.DrawLine(MakePoint(200 - CDistanceFoyer, 200), MakePoint(x, y), CL_MAGENTA);
  acanvas.DrawLine(MakePoint(200 + CDistanceFoyer, 200), MakePoint(x, y), CL_MAGENTA);
end;

procedure tmainfo.FormCreate(const sender: TObject);
begin
  FAngle := 0;
end;

procedure tmainfo.TimerExecute(const sender: TObject);
begin
  pbBox.Invalidate;
  FAngle := Succ(FAngle) mod 360;
end;

procedure tmainfo.ButtonExecute(const sender: TObject);
begin
  tmTimer.Enabled := not tmTimer.Enabled;
  if tmTimer.Enabled then
    btStart.Caption := 'Stop'
  else
    btStart.Caption := 'Start';
end;

end.
