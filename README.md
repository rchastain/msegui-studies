# MSEgui Studies

Little didactic projects using MSEide+MSEgui.

## BGRABitmap demo

How to draw with BGRABitmap in a [TPaintBox](https://msegui.net/doc/fpdoc/msesimplewidgets/tpaintbox.html).

You need [MSEgui 5](https://github.com/mse-org/mseide-msegui).

![Screenshot](bgrabitmap/splines/screenshot.png)

## Cairo demo

How to draw with Cairo in a [TWindowWidget](https://msegui.net/doc/fpdoc/msewindowwidget/twindowwidget.html).

![Screenshot](cairo/twindowwidget-ttimer/screenshot.png)

## Ellipse

Animation representing the movement of a body on an elliptical path.

![Screenshot](ellipse/screenshot.png)

## Examples

Small code examples for MSEgui documentation.

## Image Viewer

Application for quickly viewing, renaming and deleting pictures.

![Screenshot](imageviewer/screenshot.png)

## Roland's MSEide Installer

Downloads, builds and installs MSEide.

![Screenshot](mymseinstaller/screenshot.png)

## Shortcut Creator

Desktop file creator.

![Screenshot](shortcutcreator/screenshot.png)

## Spritesheet Cutter

Tool to cut a spritesheet into small images.

![Screenshot](spritesheetcutter/screenshot.png)

## Temperature Converter

Conversion from Celsius to Fahrenheit and *vice versa*.

![Screenshot](temperatureconverter/screenshot.png)

## Text Viewer

Application for quickly viewing text files.

![Screenshot](textviewer/screenshot.png)

## Wait Animation

Animation to keep you waiting.

![Screenshot](waitanimation/screenshot.png)
