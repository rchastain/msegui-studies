# TEdit example

How to create a TEdit component at runtime.

![Screenshot](screenshot.png)

## Discussion

- [freepascal.ru](http://freepascal.ru/forum/viewtopic.php?f=11&t=43228)

## Documentation

- [tedit (fpdoc)](https://msegui.net/doc/fpdoc/mseedit/tedit.html)
- [tedit (pasdoc)](https://msegui.net/doc/pasdoc/mseedit.tedit.html)
