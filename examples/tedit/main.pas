
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  //msetypes,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  //msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  //mseevent,
  mseclasses,
  msewidgets,
  mseforms,
  //mseact,
  //msedataedits,
  //msedropdownlist,
  mseedit,
  //mseificomp,
  //mseificompglob,
  //mseifiglob,
  //msestatfile,
  //msestream,
  sysutils;

type
  tmainfo = class(tmainform)
    tedit1: tedit;
    procedure mainformcreated(const sender: TObject);
    procedure mainformdestroy(const sender: TObject);
  private
    tedit2: tedit;
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm;

procedure tmainfo.mainformcreated(const sender: TObject);
begin
  tedit2 := tedit.create(self);
  tedit2.bounds_cx := 100;
  tedit2.bounds_cy := 21;
  //tedit2.frame := tcaptionframe.create(iscrollframe(self));
  tedit2.createframe;
  tedit2.frame.caption := 'Hello';
  tedit2.frame.levelo := -2;
  //tedit2.face := tface.create(self);
  tedit2.createface;
  tedit2.face.fade_color.count := 2;
  tedit2.face.fade_color[0] := cl_ltgreen;
  tedit2.face.fade_color[1] := cl_dkgreen;
  tedit2.face.fade_direction := gd_down;
  insertwidget(tedit2, makepoint(8, 40));
  tedit2.show;
end;

procedure tmainfo.mainformdestroy(const sender: TObject);
begin
  tedit2.free;
end;

end.
