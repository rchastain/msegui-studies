
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  msetypes,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msegui,
  msegraphics,
  msegraphutils,
  mseclasses,
  msewidgets,
  mseforms,
  msedataedits,
  msedropdownlist,
  sysutils,
  msesimplewidgets;

type
  tmainfo = class(tmainform)
    edit1: tdropdownlistedit;
    button1: tbutton;
    procedure mainfo_oncreate(const sender: TObject);
    procedure button_onexecute(const sender: TObject);
  end;
  
var
  mainfo: tmainfo;
  
implementation

uses
  main_mfm;

procedure tmainfo.mainfo_oncreate(const sender: TObject);
const
  a: array[0..2] of msestring = ('un', 'deux', 'trois');
(*
var
  s: msestring;
*)
begin
  (*
  edit1.dropdown.cols.addrow(['un']);
  s := 'deux';
  edit1.dropdown.cols.addrow(s);
  *)
  edit1.dropdown.cols[0].asarray := a;
  
  edit1.dropdown.itemindex := 0;
end;

procedure tmainfo.button_onexecute(const sender: TObject);
begin
  showmessage(edit1.value);
end;

end.
