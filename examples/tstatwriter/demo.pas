
program Demo;

{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
{$ifdef mswindows}{$apptype console}{$endif}

uses
{$ifdef FPC}{$ifdef unix}
  cthreads, cwstring,
{$endif}{$endif}
  sysutils,
  msetypes, // filenamety
  msestat;  // tstatwriter, tstatreader

const
  fn: filenamety = 'demo.sta';
  
var
  sw: tstatwriter;
  sr: tstatreader;
  
begin  
  try
    sw := tstatwriter.create(fn);
    with sw do
    begin
      writesection('options');
      writemsestring('a', 'abracadabra');
      writeboolean('b', TRUE);
      writeinteger('c', 2020);
    end;
  finally
    sw.free;
  end;
  
  try
    sr := tstatreader.create(fn);
    with sr do
    begin
      setsection('options');
      WriteLn(readmsestring('a', '?'));
      WriteLn(readboolean('b', FALSE));
      WriteLn(readinteger('c', 0));
    end;
  finally
    sr.free;
  end;
end.
