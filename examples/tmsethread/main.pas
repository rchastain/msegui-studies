
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  msetypes,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  msewidgets,
  mseforms,
  msesimplewidgets,
  msedispwidgets,
  mserichstring,
  msetimer,
  msethread,
  sysutils;

type
  ttestthreadcontainer = class
  private
    FThread: tmsethread;
  protected
    function Execute(thread: tmsethread): integer;
  public
    destructor Destroy; override;
    procedure Run;
  end;

  tmainfo = class(tmainform)
    btExecute: tbutton;
    tmWait: ttimer;
    pbWait: tpaintbox;
    procedure ButtonExecute(const sender: TObject);
    procedure FormCreate(const sender: TObject);
    procedure TimerExecute(const sender: TObject);
    procedure PaintBoxPaint(const sender: twidget; const acanvas: tcanvas);
    procedure FormDestroy(const sender: TObject);
  private
    FBlink: boolean;
    FThreadContainer: ttestthreadcontainer;
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm;

{ ttestthreadcontainer }

destructor ttestthreadcontainer.Destroy;
begin
  if FThread <> nil then
  begin
    FThread.Terminate;
    application.WaitForThread(FThread);
    FThread.Destroy;
  end;
  inherited;
end;

procedure ttestthreadcontainer.Run;
begin
  FThread := tmsethread.Create(@Execute);
end;

function ttestthreadcontainer.Execute(thread: tmsethread): integer;
begin
  try
    //application.Lock;
    Sleep(3000);
    mainfo.tmWait.Enabled := FALSE;
    ShowMessage('Operation completed.');
  finally
    //application.Unlock;
  end;
  result := 0;
end;

{ tmainfo }

procedure tmainfo.ButtonExecute(const sender: TObject);
begin
  tmWait.Enabled := TRUE;
  FThreadContainer.Run;
end;

procedure tmainfo.FormCreate(const sender: TObject);
begin
  FThreadContainer := ttestthreadcontainer.Create();
  FBlink := FALSE;
end;

procedure tmainfo.TimerExecute(const sender: TObject);
begin
  pbWait.Invalidate;
  FBlink := not FBlink;
end;

procedure tmainfo.PaintBoxPaint(const sender: twidget; const acanvas: tcanvas);
begin
  if FBlink then
    acanvas.FillRect(MakeRect(0, 0, sender.bounds_cx, sender.bounds_cy), CL_BLUE);
end;

procedure tmainfo.FormDestroy(const sender: TObject);
begin
  FThreadContainer.Free;
end;

end.
