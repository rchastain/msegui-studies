uses
  main_mfm,
  typinfo;

type
  TFruit = (Apple, Apricot, Cherry, Pear);

procedure tmainfo.editinit(const sender: tenumtypeedit);
begin
  sender.typeinfopo := PTypeInfo(TypeInfo(TFruit));
end;
