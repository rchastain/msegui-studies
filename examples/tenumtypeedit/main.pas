
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  msetypes,
  msegui,
  msewidgets,
  mseforms,
  msedataedits,
  mseedit,
  sysutils;

type
  tmainfo = class(tmainform)
    tenumtypeedit1: tenumtypeedit;
    procedure editinit(const sender: tenumtypeedit);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm,
  typinfo;

type
  TFruit = (Apple, Apricot, Cherry, Pear);

procedure tmainfo.editinit(const sender: tenumtypeedit);
begin
  sender.typeinfopo := PTypeInfo(TypeInfo(TFruit));
end;

end.
