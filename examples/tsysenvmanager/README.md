# TSysEnvManager example

```
[roland@localhost tsysenvmanager]$ ./demo
Parameter mandatory: -o
[roland@localhost tsysenvmanager]$ ./demo -ototo
s1 = "toto"
s2 = ""
[roland@localhost tsysenvmanager]$ ./demo -ototo tata
s1 = "toto"
s2 = "tata"
[roland@localhost tsysenvmanager]$ 
```

## Documentation

- [tsysenvmanager (fpdoc)](https://msegui.net/doc/fpdoc/msesysenv/tsysenvmanager.html)
- [tsysenvmanager (pasdoc)](https://msegui.net/doc/pasdoc/msesysenv.tsysenvmanager.html)
