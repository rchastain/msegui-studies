MSEDIR=/home/roland/Applications/mseide463-dev

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

SCRIPT_WOEXT="${SCRIPT%.*}"

#~ echo $SCRIPT_WOEXT
#~ exit 0

if [ ! -z "$1" ]
  then
    pushd "$MSEDIR"
    #~ grep -inrI "$1" "./lib/common" | tee "$SCRIPTPATH/search-$1.log"
    grep -inrI "$1" "./lib/common" | tee "$SCRIPT_WOEXT-$1.log"
    popd
fi
