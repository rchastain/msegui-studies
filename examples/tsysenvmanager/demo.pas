
program demo;

{$ifdef FPC}{$mode objfpc}{$h+}{$endif}

uses
  {$ifdef FPC}{$ifdef unix}cthreads,{$endif}{$endif}
  sysutils,
  msesys,
  msesysutils,
  msetypes,
  msesysenv,
  msestrings;

type
  argty = (arg_dest, arg_names);

const
  arguments: array[argty] of argumentdefty = (
    (kind: ak_pararg; name: 'o'; anames: nil; flags: [arf_mandatory]; initvalue: ''),
    (kind: ak_arg;    name: '';  anames: nil; flags: [];              initvalue: '')
  );

var
  sysenv: tsysenvmanager;
  s1, s2: msestring;

begin
  sysenv := tsysenvmanager.create(nil);
  try
    if sysenv.init(arguments) then
    begin
      s1 := sysenv.value[ord(arg_dest)];
      s2 := sysenv.value[ord(arg_names)];
      writestdout('s1 = "' + string(s1) + '"' + lineending);
      writestdout('s2 = "' + string(s2) + '"' + lineending);
    end;
  finally
    sysenv.destroy();
  end;
end.
