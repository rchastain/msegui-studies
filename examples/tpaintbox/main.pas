
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  //msetypes,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  //msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  //mseevent,
  mseclasses,
  //msewidgets,
  mseforms,
  msesimplewidgets,
  //msedispwidgets,
  mserichstring,
  msetimer,
  sysutils;

type
  tmainfo = class(tmainform)
    paintbox1: tpaintbox;
    procedure PaintBoxPaint(const sender: twidget; const acanvas: tcanvas);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm;

procedure tmainfo.PaintBoxPaint(const sender: twidget; const acanvas: tcanvas);
begin
  acanvas.FillRect(MakeRect(0, 0, sender.bounds_cx, sender.bounds_cy), CL_BLUE);
end;

end.
