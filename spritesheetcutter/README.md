# MSE Spritesheet Cutter

Tool to cut a spritesheet into small images.

Derived from MSE ImageList Extractor by [Almin-Soft](http://www.freepascal.ru/forum/memberlist.php?mode=viewprofile&u=3150).

## Screenshot

![Screenshot](screenshot.png)

## Result example

```
[roland@localhost spritesheetcutter]$ ls -1 chess*.png
chess_1_1.png
chess_1_2.png
chess_2_1.png
chess_2_2.png
chess_3_1.png
chess_3_2.png
chess_4_1.png
chess_4_2.png
chess_5_1.png
chess_5_2.png
chess_6_1.png
chess_6_2.png
```
