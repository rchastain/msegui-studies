
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
 msetypes,mseglob,mseguiglob,mseguiintf,mseapplication,msestat,
 msemenus,
 msegui,msegraphics,msegraphutils,
 mseevent,mseclasses,msewidgets,
 mseforms,
 msesplitter,msedataedits,
 mseedit,
 mseificomp,
 mseificompglob,
 mseifiglob,
 msestatfile,
 msestream,
 msestrings,sysutils,msesimplewidgets,mseimage,
 msebitmap,
 msedatanodes,msefiledialog,msegrids,mselistbrowser,msesys,
 msegraphicstream,mseformatpngread,mseformatjpgread,mseformatxpmread,
 mseformatbmpicoread,mseformatpnmread,mseformattgaread,mseformatpngwrite;

type
  tmainfo = class(tmainform)
    tspacer1: tspacer;
    edrowcount: trealspinedit;
    edcolcount: trealspinedit;
    bt_save: trichbutton;
    tspacer29: tspacer;
    sourceimage: timage;
    bt_close: trichbutton;
    bt_load: tbutton;
    fdopen: tfiledialog;
    mainstat: tstatfile;
    fdsave: tfiledialog;
    background: tbitmapcomp;
    timage1: timage;
   tmainmenu1: tmainmenu;
    procedure on_loadimage(const sender: TObject);
    procedure on_set_width_frameimage(const sender: TObject);
    procedure on_close_form(const sender: TObject);
    procedure on_save(const sender: TObject);
  protected
    function inputname: filenamety;
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm,
  msefileutils,
  mseformatstr;

function tmainfo.inputname: filenamety;
begin
  result := filenamebase(fdopen.controller.filename);
end;

procedure tmainfo.on_loadimage(const sender: TObject);
begin
  if fdopen.controller.execute = mr_ok then
  begin
    try
      sourceimage.bitmap.loadfromfile(fdopen.controller.filename);
      tspacer1.enabled := true;
      on_set_width_frameimage(sender);
      caption := inputname();
    except
      showmessage('Error while loading!');
    end;
  end;
end;

procedure tmainfo.on_set_width_frameimage(const sender: TObject);
var
  btmp: tmaskedbitmap;
  r: rectty;
  p: pointty;

  procedure _load(_image: timage);
  begin
    _image.bitmap.options := sourceimage.bitmap.options;
    btmp.Canvas.copyarea(sourceimage.bitmap.Canvas, r, p);
    if btmp.masked then
    begin
      btmp.mask.Canvas.copyarea(sourceimage.bitmap.mask.Canvas, r, p);
    end;
    _image.bitmap := btmp;
  end;

begin
  if not sourceimage.bitmap.hasimage then
  begin
    exit;
  end;

  btmp := tmaskedbitmap.create(bmk_rgb);
  btmp.options := sourceimage.bitmap.options;

  btmp.size := makesize(sourceimage.bitmap.width div edcolcount.asinteger, sourceimage.bitmap.height div edrowcount.asinteger);

  p.x := 0;
  p.y := 0;

  r.cx := btmp.width;
  r.cy := btmp.height;

  r.x := 0;
  r.y := 0;
  _load({timage3}timage1);

  btmp.free;
end;

procedure tmainfo.on_close_form(const sender: TObject);
begin
  close;
end;

procedure tmainfo.on_save(const sender: TObject);
var
  btmp: tmaskedbitmap;
  r: rectty;
  p: pointty;
  w, h: integer;

  procedure _load(_image: timage);
  begin
    btmp.Canvas.copyarea(_image.bitmap.Canvas, r, p);
    if btmp.masked then
    begin
      btmp.mask.Canvas.copyarea(_image.bitmap.mask.Canvas, r, p);
    end;
  end;

var
  x, y: integer;

begin
  for x := 1 to edcolcount.asinteger do
    for y := 1 to edrowcount.asinteger do
    begin
      btmp := tmaskedbitmap.create(bmk_rgb);
      try
        w := {timage3.bitmap.width} sourceimage.bitmap.width div edcolcount.asinteger;
        h := {timage3.bitmap.height} sourceimage.bitmap.height div edrowcount.asinteger;

        btmp.options := sourceimage.bitmap.options;
        btmp.size := makesize(w, h);

        r.x := w * (x - 1);
        r.y := h * (y - 1);
        r.cx := w;
        r.cy := h;

        p.x := 0;
        p.y := 0;
        _load({timage3} sourceimage);

        (*
        fdsave.controller.setfilenamelastdir(
          inputname + '_' + inttostrmse(w) + 'x' + inttostrmse(h) + '.png'
        );
        if fdsave.execute() = mr_ok then
        begin
          btmp.writetofile(fdsave.controller.filename, 'png', []);
        end;
        *)

        btmp.writetofile(unicodeformat('%s_%d_%d.png', [inputname(), x, y]), 'png', []);
      finally
        btmp.free;
      end;
    end;
end;

end.
