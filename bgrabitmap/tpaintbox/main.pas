
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  sysutils,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msegui,
  msegraphics,
  msegraphutils,
  mseclasses,
  mseforms,
  msesimplewidgets,
  msemenus,
  msewidgets,
  msetypes;

type
  tmainfo = class(tmainform)
    tpaintbox1: tpaintbox;
    tlabel1: tlabel;
    tlabel2: tlabel;
    procedure tpaintbox1_onpaint(const sender: twidget; const acanvas: tcanvas);
    procedure tmainfo_oncreate(const sender: TObject);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm,
  bgrabitmap,
  bgrabitmaptypes,
  bgragraphics;

procedure tmainfo.tpaintbox1_onpaint(const sender: twidget; const acanvas: tcanvas);
var
  bmp: tbgrabitmap;
begin
  (*
  acanvas.drawline(
    makepoint(0, 0),
    makepoint(sender.bounds_cx, sender.bounds_cy),
    cl_ltblue
  );
  *)

  bmp := tbgrabitmap.create(sender.bounds_cx, sender.bounds_cy);

  bmp.fillellipseantialias(
    sender.bounds_cx / 2, sender.bounds_cy / 2,
    sender.bounds_cx / 2, sender.bounds_cy / 2,
    cssred
    );

  bmp.draw(acanvas, 0, 0, false);
  bmp.free;
end;

procedure tmainfo.tmainfo_oncreate(const sender: TObject);
begin
  tlabel1.caption := unicodeformat('MSEgui %s', [mseguiversiontext]);
  tlabel2.caption := unicodeformat('BGRABitmap %s', [BGRABitmapVersionStr]);
end;

end.
