
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  sysutils,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msegui,
  msegraphics,
  msegraphutils,
  mseclasses,
  mseforms,
  msesimplewidgets,
  msemenus,
  msewidgets;

type
  tmainfo = class(tmainform)
    tpaintbox1: tpaintbox;
    tlabel1: tlabel;
    tlabel2: tlabel;
    procedure tpaintbox1_onpaint(const sender: twidget; const acanvas: tcanvas);
    procedure tmainfo_oncreate(const sender: TObject);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm,
  bgrabitmap,
  bgrabitmaptypes,
  bgragraphics;

(* https://wiki.freepascal.org/BGRABitmap_tutorial_5 *)

procedure tmainfo.tpaintbox1_onpaint(const sender: twidget; const acanvas: tcanvas);
var
  temp, tex, mask: TBGRABitmap;
begin
  temp := TBGRABitmap.Create(640, 480 {,ColorToBGRA(ColorToRGB(clBtnFace))});

  //loading and scaling texture
  tex := TBGRABitmap.Create('texture.png');
  BGRAReplace(tex, tex.Resample(128, 80));

  //show image in the upper-left corner
  temp.PutImage(10, 10, tex, dmDrawWithTransparency);

  //create a mask with ellipse and rectangle
  mask := TBGRABitmap.Create(128, 80, BGRABlack);
  mask.FillEllipseAntialias(40, 40, 30, 30, BGRAWhite);
  mask.FillRectAntialias(60, 40, 100, 70, BGRAWhite);

  //show mask in the upper-right corner
  temp.PutImage(150, 10, mask, dmDrawWithTransparency);

  //apply the mask to the image
  tex.ApplyMask(mask);

  //show the result image in the lower-left corner
  temp.PutImage(10, 100, tex, dmDrawWithTransparency);

  mask.Free;
  tex.Free;

  //show everything on the screen
  temp.Draw(acanvas, 0, 0, false);
  temp.Free;
end;

procedure tmainfo.tmainfo_oncreate(const sender: TObject);
begin
  tlabel1.caption := unicodeformat('MSEgui %s', [mseguiversiontext]);
  tlabel2.caption := unicodeformat('BGRABitmap %s', [BGRABitmapVersionStr]);
end;

end.
