
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  sysutils,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msegui,
  msegraphics,
  msegraphutils,
  mseclasses,
  mseforms,
  msesimplewidgets,
  msemenus,
  msewidgets;

type
  tmainfo = class(tmainform)
    tpaintbox1: tpaintbox;
    tlabel1: tlabel;
    tlabel2: tlabel;
    procedure tpaintbox1_onpaint(const sender: twidget; const acanvas: tcanvas);
    procedure tmainfo_oncreate(const sender: TObject);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm,
  bgrabitmap,
  bgrabitmaptypes,
  bgragraphics,
  bgracanvas2d;

(* https://wiki.freepascal.org/BGRABitmap_tutorial_14 *)

procedure tmainfo.tpaintbox1_onpaint(const sender: twidget; const acanvas: tcanvas);
var
  bmp: TBGRABitmap;
  ctx: TBGRACanvas2D;
begin
  bmp := TBGRABitmap.Create(sender.bounds_cx, sender.bounds_cy);

  ctx := bmp.Canvas2D;
  ctx.fillStyle('rgb(240,128,0)');
  ctx.fillRect(30, 30, 80, 60);
  ctx.strokeRect(50, 50, 80, 60);

  bmp.Draw(acanvas, 0, 0, FALSE);
  bmp.Free;
end;

procedure tmainfo.tmainfo_oncreate(const sender: TObject);
begin
  tlabel1.caption := unicodeformat('MSEgui %s', [mseguiversiontext]);
  tlabel2.caption := unicodeformat('BGRABitmap %s', [BGRABitmapVersionStr]);
end;

end.
