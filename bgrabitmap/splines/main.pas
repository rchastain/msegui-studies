
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  sysutils,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msegui,
  msegraphics,
  msegraphutils,
  mseclasses,
  mseforms,
  msesimplewidgets,
  msemenus,
  msewidgets;

type
  tmainfo = class(tmainform)
    tpaintbox1: tpaintbox;
    tlabel1: tlabel;
    tlabel2: tlabel;
    procedure tpaintbox1_onpaint(const sender: twidget; const acanvas: tcanvas);
    procedure tmainfo_oncreate(const sender: TObject);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm,
  bgrabitmap,
  bgrabitmaptypes,
  bgragraphics;

procedure tmainfo.tpaintbox1_onpaint(const sender: twidget; const acanvas: tcanvas);
var
  image: TBGRABitmap;
  pts: array of TPointF;
  storedSpline: array of TPointF;
  c: TBGRAPixel;
begin
    //image := TBGRABitmap.Create(ClientWidth,ClientHeight,ColorToBGRA(ColorToRGB(clBtnFace)));
    //c := ColorToBGRA(ColorToRGB(clWindowText));
  image := TBGRABitmap.Create(sender.bounds_cx, sender.bounds_cy);
  c := cssred;
    //rectangular polyline
  setlength(pts, 4);
  pts[0] := PointF(50, 50);
  pts[1] := PointF(150, 50);
  pts[2] := PointF(150, 150);
  pts[3] := PointF(50, 150);
  image.DrawPolylineAntialias(pts, BGRA(255, 0, 0, 150), 1);

    //compute spline points and draw as a polyline
  storedSpline := image.ComputeOpenedSpline(pts, ssVertexToSide);
  image.DrawPolylineAntialias(storedSpline, c, 1);

  image.Draw(acanvas, 0, 0, false);
  image.free;
end;

procedure tmainfo.tmainfo_oncreate(const sender: TObject);
begin
  tlabel1.caption := unicodeformat('MSEgui %s', [mseguiversiontext]);
  tlabel2.caption := unicodeformat('BGRABitmap %s', [BGRABitmapVersionStr]);
end;

end.
