
## Compile MSEgui project

UNITS=units

#~ rm -f $UNITS/*.*
mkdir -p $UNITS

## MSEgui directory
MSEDIR=/home/roland/Documents/git/rchastain/mseide-msegui

if [ ! -d $MSEDIR ] ;
then
echo "Cannot find directory $MSEDIR"
exit 0
fi

## Program source file
SOURCE=source/myinstaller.pas

if [ ! -e $SOURCE ] ;
then
echo "Cannot find $SOURCE"
exit 0
fi

if [ "$1" = "debug" ] ; then
  OPTIONS="-Xg -gl -O-"
else
  OPTIONS="-O2 -XX -Xs -CX -B"
fi

echo "Compiling project"

fpc \
-Fu$MSEDIR/lib/common/* \
-Fu$MSEDIR/lib/common/kernel \
-Fi$MSEDIR/lib/common/kernel \
-Fu$MSEDIR/lib/common/kernel/linux \
-Fu$MSEDIR/lib/addon/* \
-Fi$MSEDIR/lib/addon/* \
-FU$UNITS/ \
-l -Mobjfpc -Sh -o./myinstaller $OPTIONS $SOURCE \
&> build.log 
