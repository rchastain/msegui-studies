# Roland's MSEide Installer

Downloads, builds and installs MSEide.

Installation consists only in creating shortcuts: one on desktop, another in applications menu. The MSEide executable stays where it has been built.

Each MSEide executable installed uses its own configuration file.

Work in progress. For now, Linux only.

![Screenshot](screenshot.png)
