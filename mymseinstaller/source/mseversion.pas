
unit mseversion;

{
lib/common/kernel/msegui.pas:37: mseguiversiontext = '4.6.3';
apps/ide/main.pas:49: versiontext = '4.6.3';
}

interface

function getmseideversion(const afilepath: string; var aversion: string): boolean;

implementation

uses
  classes, regexpr;

function readtext(const afilename: ansistring): ansistring;
var
  fs: tfilestream;
begin
  fs := tfilestream.create(afilename, fmopenread);
  try
    setlength(result, fs.size div sizeof(ansichar));
    fs.readbuffer(pansichar(result)^, fs.size);
  finally
    fs.free;
  end;
end;

function getmseideversion(const afilepath: string; var aversion: string): boolean;
var
  ltext: string;
  expr: TRegExpr;
begin
  ltext := readtext(afilepath);
  
  expr := TRegExpr.Create('versiontext\s*=\s*''(\d+\.\d+\.\d+)''');
  
  if expr.Exec(ltext) then
  begin
    result := true;
    aversion := expr.Match[1];
  end else
  begin
    result := false;
    aversion := '';
  end;
  
  expr.Free;
end;

end.
