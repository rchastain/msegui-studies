
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  msetypes,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  //msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  //mseevent,
  mseclasses,
  msewidgets,
  mseforms,
  //mseact,
  msedataedits,
  msedropdownlist,
  mseedit,
  //mseificomp,
  //mseificompglob,
  //mseifiglob,
  //msestatfile,
  msestream,
  sysutils,
  //msebitmap,
  //msedatanodes,
  //msedragglob,
  msefiledialog,
  msegrids,
  //msegridsglob,
  mselistbrowser,
  msesys,
  msesimplewidgets,
  //mseeditglob,
  mserichstring,
  msetextedit,
  //msedispwidgets,
  //mseprocutils,
  msepipestream,
  //mseprocess,
  mseterminal,
  msewidgetgrid,
  msesysintf,
  msetimer,
  msegraphedits{,
  msescrollbar};

type
  tmainfo = class(tmainform)
    edit_folderpath: tdirdropdownedit;
    button_install: tbutton;
    grid_debug: tstringgrid;
    edit_foldername: tedit;
    button_quit: tbutton;
    grid_terminal: twidgetgrid;
    terminal1: tterminal;
    button_download: tbutton;
    button_build: tbutton;
    timer1: ttimer;
    edit_branch: tedit;
    edit_repository: tdropdownlistedit;
    edit_specific: tbooleanedit;
    procedure mainfo_oncreate(const sender: TObject);
    procedure folder_onchange(const sender: TObject);
    procedure install_onexecute(const sender: TObject);
    procedure quit_onexecute(const sender: TObject);
    procedure download_onexecute(const sender: TObject);
    procedure build_onexecute(const sender: TObject);
    procedure timer1_onexecute(const sender: TObject);
    procedure edit_specific_onchange(const sender: TObject);
  private
    ffolderfullname: msestring;
    fmseidefullname: msestring;
    fsourcefullname: msestring;
    procedure info(const atext: msestring);
    procedure execgit(const agitrepos, atargetdir, abranch: msestring);
    procedure execrm(const adir: msestring);
    procedure execfpc(const adir: msestring);
    procedure createshortcuts(const adir: msestring);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm,
  mseversion,
  desktopfile;

procedure tmainfo.info(const atext: msestring);
begin
  grid_debug.appendrow(atext);
end;

procedure tmainfo.execgit(const agitrepos, atargetdir, abranch: msestring);
var
  lcmd: msestring;
begin
  info(unicodeformat('execgit(%s,%s,%s)', [agitrepos, atargetdir, abranch]));
  
  if length(abranch) > 0 then
    lcmd := unicodeformat('git clone %s %s --branch %s -c advice.detachedHead=false', [agitrepos, atargetdir, abranch])
  else
    lcmd := unicodeformat('git clone %s %s', [agitrepos, atargetdir]);
  
  terminal1.execprog(lcmd, '', nil, nil);
 {terminal1.waitforprocess;}
end;

procedure tmainfo.execrm(const adir: msestring);
var
  lcmd: msestring;
begin
  info(unicodeformat('execrm(%s)', [adir]));
  lcmd := unicodeformat('rm -rfv %s', [adir]);
  terminal1.execprog(lcmd, '', nil, nil);
 {terminal1.waitforprocess;}
end;

procedure tmainfo.execfpc(const adir: msestring);
var
  lstream: ttextstream;
  lunits, lcmd, lsh: msestring;
begin
  info(unicodeformat('execfpc(%s)', [adir]));

  lunits := adir + '/units';
  lsh := extractfilepath(sys_getapplicationpath) + 'build-' + edit_foldername.text + '.sh';
  lstream := ttextstream.create(lsh, fm_create);
  lstream.writeln(unicodeformat('mkdir -p %s', [lunits]));
  lstream.writeln('fpc \');
  lstream.writeln(unicodeformat('-Fu%s/lib/common/* \', [adir]));
  lstream.writeln(unicodeformat('-Fu%s/lib/common/kernel \', [adir]));
  lstream.writeln(unicodeformat('-Fi%s/lib/common/kernel \', [adir]));
  lstream.writeln(unicodeformat('-Fu%s/lib/common/kernel/linux \', [adir]));
  lstream.writeln(unicodeformat('-Fu%s/lib/addon/* \', [adir]));
  lstream.writeln(unicodeformat('-Fi%s/lib/addon/* \', [adir]));
  lstream.writeln(unicodeformat('-FU%s \', [lunits]));
  lstream.writeln('-Xg -l -Mobjfpc -Sh -Fcutf8 -gl -O- \');
  lstream.writeln(unicodeformat('%s/apps/ide/mseide.pas', [adir]));
  lstream.close;
  lstream.free;

  lcmd := unicodeformat('sh %s', [lsh]);
  terminal1.execprog(lcmd, '', nil, nil);
 {terminal1.waitforprocess;}
end;

procedure tmainfo.createshortcuts(const adir: msestring);
var
  ltargetdir, lcmd, ldesktop: msestring;
  lversion: string;
begin
  info(unicodeformat('createshortcuts(%s)', [adir]));
  
  lversion := '?.?.?';
  if getmseideversion(string(adir + '/apps/ide/main.pas'), lversion) then
    info('Found MSEgui version [' + msestring(lversion) + ']');   

  ldesktop := extractfilepath(sys_getapplicationpath) + edit_foldername.text + '.desktop';

  createdesktopfile(
    ldesktop,
    unicodeformat('MSEide %s', [lversion]),
    unicodeformat('%s/apps/ide/mseide --globstatfile=%s/apps/ide/mseide.sta %%F', [adir, adir]),
    unicodeformat('%s/msegui_48.png', [adir]),
    unicodeformat('%s/apps/ide', [adir])
  );

  ltargetdir := msestring(expandfilename('~/') + 'Desktop');
  if not directoryexists(ltargetdir) then
  begin
    info('Cannot find directory [' + ltargetdir + ']');
    ltargetdir := msestring(expandfilename('~/') + 'Bureau');
    if not directoryexists(ltargetdir) then
    begin
      info('Cannot find directory [' + ltargetdir + ']');
      exit;
    end;
  end;

  lcmd := unicodeformat('sudo cp -fv %s %s/', [ldesktop, ltargetdir]);
  terminal1.execprog(lcmd, '', nil, nil);
  terminal1.waitforprocess;

  ltargetdir := msestring(expandfilename('~/')) + '.local/share/applications';

  if not directoryexists(ltargetdir) then
  begin
    info('Cannot find directory [' + ltargetdir + ']');
    exit;
  end;

  lcmd := unicodeformat('sudo cp -fv %s %s/', [ldesktop, ltargetdir]);
  terminal1.execprog(lcmd, '', nil, nil);
  terminal1.waitforprocess;
end;

procedure tmainfo.mainfo_oncreate(const sender: TObject);
const
  crepositories: array[0..2] of msestring = (
    'https://github.com/mse-org/mseide-msegui.git',
    'https://gitlab.com/rchastain/mseide-msegui.git',
    'https://github.com/circular17/mseide-msegui.git'
    );
(*
var
  i: integer;
*)
begin
  info('My MSEide Installer (' + {$I %DATE%} + ', ' + {$I %TIME%} + ', FPC ' + {$I %FPCVERSION%} + ', ' + {$I %FPCTARGETOS%} + ')');

  (*
  for i := low(crepositories) to high(crepositories) do
    edit_repository.dropdown.cols.addrow(crepositories[i]);
  *)
  edit_repository.dropdown.cols[0].asarray := crepositories;

  edit_repository.dropdown.itemindex := 0;

  edit_folderpath.value := msestring(expandfilename('~/'));
  edit_foldername.text := msestring('mseide-' + formatdatetime('yymmddhhnn', now));
end;

procedure tmainfo.folder_onchange(const sender: TObject);
begin
  ffolderfullname := concat(edit_folderpath.value, edit_foldername.text);
  fmseidefullname := unicodeformat('%s/apps/ide/mseide', [ffolderfullname]);
  fsourcefullname := concat(fmseidefullname, '.pas');
{$IFDEF MSWINDOWS}
  fmseidefullname := concat(fmseidefullname, '.exe');
{$ENDIF}

  info(unicodeformat('folder_onchange [ffolderfullname=%s]', [ffolderfullname]));

  timer1.enabled := true;
end;

procedure tmainfo.install_onexecute(const sender: TObject);
begin
  createshortcuts(ffolderfullname);
end;

procedure tmainfo.quit_onexecute(const sender: TObject);
begin
  application.terminate;
end;

procedure tmainfo.download_onexecute(const sender: TObject);
begin
  (*
  if directoryexists(ffolderfullname) then
  begin
    info('Directory already exists [' + ffolderfullname + ']');

    if askyesno('Delete existing directory?', 'Delete') then
      execrm(ffolderfullname);

  end else
  *)
  if edit_specific.value then
  begin
    if length(edit_branch.text) > 0 then
      execgit(edit_repository.text, ffolderfullname, edit_branch.text)
    else
      info('Branch or tag not specified');
  end else
    execgit(edit_repository.text, ffolderfullname, '');
end;

procedure tmainfo.build_onexecute(const sender: TObject);
begin
  execfpc(ffolderfullname);
end;

procedure tmainfo.timer1_onexecute(const sender: TObject);
begin
  if terminal1.running then
  begin
    button_download.enabled := false;
    button_build.enabled := false;
    button_install.enabled := false;
  end else
  begin
    button_download.enabled := not directoryexists(ffolderfullname);
    button_build.enabled := fileexists(fsourcefullname);
    button_install.enabled := fileexists(fmseidefullname);
  end;
end;

procedure tmainfo.edit_specific_onchange(const sender: TObject);
begin
  edit_branch.enabled := edit_specific.value;
end;

end.
