
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  msetypes,
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  msewidgets,
  mseforms,
  mseact,
  msedataedits,
  msedropdownlist,
  mseedit,
  mseificomp,
  mseificompglob,
  mseifiglob,
  msefileutils,
  msestatfile,
  msestream,
  sysutils,
  msebitmap,
  msedatanodes,
  msedragglob,
  msefiledialog,
  msegrids,
  msegridsglob,
  mselistbrowser,
  mseprocutils,
  msesys,
  msesysintf,
  msesimplewidgets,
  msegraphedits,
  msescrollbar;

type
  tmainfo = class(tmainform)
    editfilename: tstringedit;
    edittargetdir: tfilenameedit;
    edittarget: tfilenameedit;
    editicon: tfilenameedit;
    editname: tstringedit;
    editcomment: tstringedit;
    editmaincategory: tenumtypeedit;
    editadditionalcategory: tenumtypeedit;
    buttoncreate: tbutton;
    menu: tmainmenu;
    procedure editmaintcategoryinit(const sender: tenumtypeedit);
    procedure editadditionalcategoryinit(const sender: tenumtypeedit);
    procedure mainformcreate(const sender: TObject);
    procedure edittargetchange(const sender: TObject);
    procedure buttoncreateexecute(const sender: TObject);
    procedure mainformcreated(const sender: TObject);
    procedure itemquitexecute(const sender: TObject);
   procedure itemaboutexecute(const sender: TObject);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm,
  typinfo,
  baseunix,
  desktopfile, (* CreateDesktopFile *)
  desktoptypes, (* TEntryType, TMainCategory, TAdditionalCategory *)
  permissions,
  log,
  msei18nutils,
  msestrings;

resourcestring
  rs_about =
    'MSEgui Shortcut Creator.' + lineending + lineending +
    'Shortcut Creator for Linux.' + lineending + lineending +
    'Helps you to create a shortcut to any application, on your desktop and in the applications menu.';

procedure tmainfo.editmaintcategoryinit(const sender: tenumtypeedit);
begin
  LogLn('tmainfo.editmaintcategoryinit');
  sender.typeinfopo := PTypeInfo(TypeInfo(TMainCategory));
end;

procedure tmainfo.editadditionalcategoryinit(const sender: tenumtypeedit);
begin
  LogLn('tmainfo.editadditionalcategoryinit');
  sender.typeinfopo := PTypeInfo(TypeInfo(TAdditionalCategory));
end;

procedure tmainfo.mainformcreate(const sender: TObject);
begin
  LogLn('tmainfo.mainformcreate');
  //editmaincategory.value := 0;
  editadditionalcategory.value := 0;
  editicon.controller.filter := '"*.jpg" "*.jpeg" "*.bmp" "*.ico" "*.png" "*.pnm" "*.pgm" "*.pbm" "*.tga" "*.xpm"';
end;

procedure tmainfo.edittargetchange(const sender: TObject);
var
  s: msestring;
begin
  LogLn('tmainfo.edittargetchange');

  s := msestring(ChangeFileExt(ExtractFileName(RawByteString(edittarget.value)), ''));
  if length(editname.value) = 0 then
    editname.value := s;
  editfilename.value := s + '.desktop';

  if not IsExecutable(string(edittarget.value)) then
    ShowMessage('The file is not executable.');
end;

procedure tmainfo.buttoncreateexecute(const sender: TObject);
const
  CPermission: TMode =
  S_IRWXO or // Read, write, execute by others
  S_IRWXG or // Read, write, execute by groups
  S_IRWXU;   // Read, write, execute by user
var
  LFileName, LHomeDir, LDesktopDir, LTempDir, LTempFilePath, LDest1, LDest2: filenamety;
  i: integer;
begin
  LogLn('tmainfo.buttoncreateexecute');
  
  if (editmaincategory.value       < 0)
  or (editadditionalcategory.value < 0) then
  begin
    ShowMessage('You must select a value for categories. Operation aborted.');
    Exit;
  end;

  LFileName     := '/' + editfilename.value;
  LHomeDir      := msestring(getenvironmentvariable('HOME'));           (* Dossier personnel                     *)
  LDesktopDir   := LHomeDir + '/Desktop';                               (* Bureau                                *)
  LTempDir      := msestring(getenvironmentvariable('TMPDIR'));         (* Dossier temporaire                    *)
  LTempFilePath := LTempDir + LFileName;                                (* Chemin du fichier .desktop temporaire *)
  LDest1        := LDesktopDir + LFileName;                             (* Chemin définitif du fichier           *)
  LDest2        := LHomeDir + '/.local/share/applications' + LFileName; (* Chemin définitif du fichier, bis      *)

  LogLn('Home directory: ' + LHomeDir);
  LogLn('Desktop: '        + LDesktopDir);
  LogLn('Temporary file: ' + LTempFilePath);

  LogLn('Creating temporary file...');
  
  CreateDesktopFile(
    etApplication,
    editname.value,
    edittargetdir.value,
    edittarget.value,
    editicon.value,
    FALSE,
    editcomment.value,
    TMainCategory(editmaincategory.value),
    TAdditionalCategory(editadditionalcategory.value),
    LTempFilePath
  );
  
  if FileExists(LTempFilePath) then
  begin
    LogLn('Copying temporary file to ' + LDest1);
    
    execmse('cp -f ' + LTempFilePath + ' ' + LDest1);
  
    LogLn('Copying temporary file to: ' + LDest2);
    
    execmse('cp -f ' + LTempFilePath + ' ' + LDest2);
  
    LogLn('Setting permissions...');
    
    FPchmod(rawbytestring(tosysfilepath(LDest1)), CPermission);
    FPchmod(rawbytestring(tosysfilepath(LDest2)), CPermission);
  
    LogLn('Done.');
  end else
  begin
    LogLn('Cannot find temporary file.');
  end;
end;

procedure tmainfo.mainformcreated(const sender: TObject);
begin
  LogLn('tmainfo.mainformcreated');
  
  if (ParamCount = 1) and FileExists(ParamStr(1)) then
  begin
    edittarget.value := msestring(ParamStr(1));
    edittargetdir.value := msestring(ExtractFileDir(ParamStr(1)));
    editicon.controller.lastdir := edittargetdir.value;
  end;
  
  loadlangunit('shortcutcreator_fr');
end;

procedure tmainfo.itemquitexecute(const sender: TObject);
begin
  application.terminated := TRUE;
end;

procedure tmainfo.itemaboutexecute(const sender: TObject);
begin
  showmessage(utf8tostring(rs_about));
end;

end.
