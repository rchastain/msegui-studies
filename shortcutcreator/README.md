# MSEgui Shortcut Creator

Shortcut Creator for Linux.

Helps you to create a shortcut to any application, on your desktop and in the applications menu. 

![Screenshot](screenshot.png)

Uses the **tenumtypeedit** class.

- [tenumtypeedit (fpdoc)](https://msegui.net/doc/fpdoc/msedataedits/tenumtypeedit.html) 
- [tenumtypeedit (pasdoc)](https://msegui.net/doc/pasdoc/msedataedits.tenumtypeedit.html)
