
program shortcutcreator;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

{$IFNDEF linux}
{$FATAL This program is for Linux only.}
{$ENDIF}

uses
{$IFDEF FPC}{$IFDEF linux}
  cthreads,
{$ENDIF}{$ENDIF}
  msegui,
  main,
  log;

const
  CBuildInfos = {$I %DATE%} + ', ' + {$I %TIME%} + ', FPC ' + {$I %FPCVERSION%} + ' for ' + {$I %FPCTARGETOS%};
  CRewriteLog = TRUE;
    
begin
  LogLn('MSEgui Shortcut Creator (' + CBuildInfos + ')', CRewriteLog);
  application.createform(tmainfo, mainfo);
  application.run;
end.
