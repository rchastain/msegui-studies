
unit Permissions;

interface

function IsExecutable(const AFilename: string): boolean;

implementation

uses
  SysUtils, BaseUnix;
  
(* http://free-pascal-general.1045716.n5.nabble.com/How-to-get-the-permission-f-a-file-tp5721836p5721838.html *)

function IsExecutable(const AFilename: string): boolean;
var
  LStat: stat;
begin
  // first check AFilename is not a directory and then check if executable
  Result:= (FpStat(AFilename, LStat) <> -1)
    and FPS_ISREG(LStat.st_mode)
    and (FpAccess(AFilename, X_OK) = 0);
end;

(*
(From LazFileUtils unit of Lazarus)

Bart
*)

end.
