
unit DesktopFile;

(* Création d'un fichier .desktop *)

interface

uses
  baseunix,
  sysutils,
  classes,
  msestrings,
  msesys,
  msefileutils,
  msesysintf,
  msetypes,
  mseprocutils,
  msestream,
  desktoptypes; (* TEntryType, TMainCategory, TAdditionalCategory *)

procedure CreateDesktopFile(
  const AEntryType: TEntryType;
  const AName: msestring;
  const APath, AExec, AIcon: filenamety;
  const ATerminal: boolean;
  const AComment: msestring;
  const AMainCategory: TMainCategory;
  const AAdditionalCategory: TAdditionalCategory;
  const AFileName: filenamety
);

implementation

procedure CreateDesktopFile(
  const AEntryType: TEntryType;
  const AName: msestring;
  const APath, AExec, AIcon: filenamety;
  const ATerminal: boolean;
  const AComment: msestring;
  const AMainCategory: TMainCategory;
  const AAdditionalCategory: TAdditionalCategory;
  const AFileName: filenamety
);
const
  CBoolStr: array[boolean] of msestring = (
    'false',
    'true'
  );
  COptionalSemicolon: array[boolean] of msestring = ('', ';');
var
  LStream: ttextstream;
begin
  LStream := ttextstream.Create(AFileName, fm_create);
  LStream.WriteLn('[Desktop Entry]');
  LStream.WriteLn('Type=' + CEntryTypeStr[AEntryType]);
  LStream.WriteLn('Encoding=UTF-8');
  LStream.WriteLn('Name=' + AName);
  LStream.WriteLn('Comment=' + AComment);
  LStream.WriteLn('Path=' + APath);
  LStream.WriteLn('Exec=' + AExec + ' %f');
  LStream.WriteLn('Icon=' + AIcon);
  LStream.WriteLn('Terminal=' + CBoolStr[ATerminal]);
  LStream.WriteLn('Categories=' + CMainCategoryStr[AMainCategory] + ';' + CAdditionalCategoryStr[AAdditionalCategory] + COptionalSemicolon[AAdditionalCategory <> acNone]);
  LStream.Close;
  LStream.Free;
end;

end.
