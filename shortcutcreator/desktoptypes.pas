
unit DesktopTypes;

interface

uses
  msetypes;
  
type
  TEntryType = (etApplication, etLink, etDirectory);
  
  (* https://specifications.freedesktop.org/menu-spec/latest/apa.html *)
  TMainCategory = (
    mcAudioVideo,  // Application for presenting, creating, or processing multimedia (audio/video)	 
    mcAudio,       // An audio application	Desktop entry must include AudioVideo as well
    mcVideo,       // A video application	Desktop entry must include AudioVideo as well
    mcDevelopment, // An application for development	 
    mcEducation,   // Educational software	 
    mcGame,        // A game	 
    mcGraphics,    // Application for viewing, creating, or processing graphics	 
    mcNetwork,     // Network application such as a web browser	 
    mcOffice,      // An office type application	 
    mcScience,     // Scientific software	 
    mcSettings,    // Settings applications	Entries may appear in a separate menu or as part of a "Control Center"
    mcSystem,      // System application, "System Tools" such as say a log viewer or network monitor	 
    mcUtility      // Small utility application, "Accessories"
  );

  (* https://specifications.freedesktop.org/menu-spec/latest/apas02.html *)
  TadditionalCategory = (
    acNone,
    acBuilding, // A tool to build applications Development
    acDebugger, // A tool to debug applications Development
    acIDE, // IDE application Development
    acGUIDesigner, // A GUI designer application  Development
    acProfiling, // A profiling tool  Development
    acRevisionControl, // Applications like cvs or subversion Development
    acTranslation, // A translation tool  Development
    acCalendar, // Calendar application Office
    acContactManagement, // E.g. an address book  Office
    acDatabase, // Application to manage a database Office or Development or AudioVideo
    acDictionary, // A dictionary Office or TextTools
    acChart, // Chart application Office
    acEmail, // Email application Office or Network
    acFinance, // Application to manage your finance  Office
    acFlowChart, // A flowchart application Office
    acPDA, // Tool to manage your PDA Office
    acProjectManagement, // Project management application  Office or Development
    acPresentation, // Presentation software  Office
    acSpreadsheet, // A spreadsheet Office
    acWordProcessor, // A word processor  Office
    ac2DGraphics, // 2D based graphical application Graphics
    acVectorGraphics, // Application for viewing, creating, or processing vector graphics Graphics;2DGraphics
    acRasterGraphics, // Application for viewing, creating, or processing raster (bitmap) graphics  Graphics;2DGraphics
    ac3DGraphics, // Application for viewing, creating, or processing 3-D graphics  Graphics
    acScanning, // Tool to scan a file/text Graphics
    acOCR, // Optical character recognition application Graphics;Scanning
    acPhotography, // Camera tools, etc.  Graphics or Office
    acPublishing, // Desktop Publishing applications and Color Management tools Graphics or Office
    acViewer, // Tool to view e.g. a graphic or pdf file  Graphics or Office
    acTextTools, // A text tool utility Utility
    acDesktopSettings, // Configuration tool for the GUI  Settings
    acHardwareSettings, // A tool to manage hardware components, like sound cards, video cards or printers  Settings
    acPrinting, // A tool to manage printers  HardwareSettings;Settings
    acPackageManager, // A package manager application  Settings
    acDialup, // A dial-up program  Network
    acInstantMessaging, // An instant messaging client  Network
    acChat, // A chat client  Network
    acIRCClient, // An IRC client Network
    acFeed, // RSS, podcast and other subscription based contents Network
    acFileTransfer, // Tools like FTP or P2P programs Network
    acHamRacio, // HAM racio software Network or Audio
    acNews, // A news reacer or a news ticker Network
    acP2P, // A P2P program Network
    acRemoteAccess, // A tool to remotely manage your PC  Network
    acTelephony, // Telephony via PC  Network
    acTelephonyTools, // Telephony tools, to dial a number, manage PBX, ... Utility
    acVideoConference, // Video Conference software Network
    acWebBrowser, // A web browser  Network
    acWebDevelopment, // A tool for web developers  Network or Development
    acMidi, // An app related to MIDI AudioVideo;Audio
    acMixer, // Just a mixer  AudioVideo;Audio
    acSequencer, // A sequencer AudioVideo;Audio
    acTuner, // A tuner AudioVideo;Audio
    acTV, // A TV application AudioVideo;Video
    acAudioVideoEditing, // Application to edit audio/video files Audio or Video or AudioVideo
    acPlayer, // Application to play audio/video files  Audio or Video or AudioVideo
    acRecorder, // Application to record audio/video files  Audio or Video or AudioVideo
    acDiscBurning, // Application to burn a disc  AudioVideo
    acActionGame, // An action game Game
    acAdventureGame, // Adventure style game  Game
    acArcadeGame, // Arcade style game  Game
    acBoardGame, // A board game  Game
    acBlocksGame, // Falling blocks game  Game
    acCardGame, // A card game  Game
    acKidsGame, // A game for kids  Game
    acLogicGame, // Logic games like puzzles, etc Game
    acRolePlaying, // A role playing game Game
    acShooter, // A shooter game  Game
    acSimulation, // A simulation game  Game
    acSportsGame, // A sports game  Game
    acStrategyGame, // A strategy game  Game
    acArt, // Software to teach arts  Education or Science
    acConstruction, // Education or Science
    acMusic, // Musical software  AudioVideo or Education
    acLanguages, // Software to learn foreign languages Education or Science
    acArtificialIntelligence, // Artificial Intelligence software Education or Science
    acAstronomy, // Astronomy software  Education or Science
    acBiology, // Biology software  Education or Science
    acChemistry, // Chemistry software  Education or Science
    acComputerScience, // ComputerScience software Education or Science
    acDataVisualization, // Data visualization software Education or Science
    acEconomy, // Economy software  Education or Science
    acElectricity, // Electricity software  Education or Science
    acGeography, // Geography software  Education or Science
    acGeology, // Geology software  Education or Science
    acGeoscience, // Geoscience software, GIS Education or Science
    acHistory, // History software  Education or Science
    acHumanities, // Software for philosophy, psychology and other humanities Education or Science
    acImageProcessing, // Image Processing software Education or Science
    acLiterature, // Literature software  Education or Science
    acMaps, // Sofware for viewing maps, navigation, mapping, GPS Education or Science or Utility
    acMath, // Math software  Education or Science
    acNumericalAnalysis, // Numerical analysis software Education;Math or Science;Math
    acMedicalSoftware, // Medical software  Education or Science
    acPhysics, // Physics software  Education or Science
    acRobotics, // Robotics software  Education or Science
    acSpirituality, // Religious and spiritual software, theology Education or Science or Utility
    acSports, // Sports software  Education or Science
    acParallelComputing, // Parallel computing software Education;ComputerScience or Science;ComputerScience
    acAmusement, // A simple amusement   
    acArchiving, // A tool to archive/backup data Utility
    acCompression, // A tool to manage compressed data/archives Utility;Archiving
    acElectronics, // Electronics software, e.g. a circuit designer  
    acEmulator, // Emulator of another platform, such as a DOS emulator System or Game
    acEngineering, // Engineering software, e.g. CAD programs  
    acFileTools, // A file tool utility Utility or System
    acFileManager, // A file manager  System;FileTools
    acTerminalEmulator, // A terminal emulator application  System
    acFilesystem, // A file system tool System
    acMonitor, // Monitor application/applet that monitors some resource or activity  System or Network
    acSecurity, // A security tool  Settings or System
    acAccessibility, // Accessibility Settings or Utility
    acCalculator, // A calculator Utility
    acClock, // A clock application/applet  Utility
    acTextEditor, // A text editor  Utility
    acDocumentation, // Help or documentation  
    acAdult, // Application handles adult or explicit material   
    acCore, // Important application, core to the desktop such as a file manager or a help browser   
    acKDE, // Application based on KDE libraries  QT
    acGNOME, // Application based on GNOME libraries  GTK
    acXFCE, // Application based on XFCE libraries  GTK
    acGTK, // Application based on GTK+ libraries  
    acQt, // Application based on Qt libraries   
    acMotif, // Application based on Motif libraries   
    acJava, // Application based on Java GUI libraries, such as AWT or Swing   
    acConsoleOnly // Application that only works inside a terminal (text-based or command line application)  
  );

const
  CEntryTypeStr: array[TEntryType] of msestring = (
    'Application',
    'Link',
    'Directory'
  );
  
  CMainCategoryStr: array[TMainCategory] of msestring = (
    'AudioVideo',	 
    'Audio',
    'Video',
    'Development',	 
    'Education',	 
    'Game',	 
    'Graphics',	 
    'Network',	 
    'Office',
    'Science',
    'Settings', 
    'System',	 
    'Utility'
  );
  
  CadditionalCategoryStr: array[TadditionalCategory] of msestring = (
    '',
    'Building',
    'Debugger',
    'IDE',
    'GUIDesigner',
    'Profiling',
    'RevisionControl',
    'Translation',
    'Calendar',
    'ContactManagement',
    'Database',
    'Dictionary',
    'Chart',
    'Email',
    'Finance',
    'FlowChart',
    'PDA',
    'ProjectManagement',
    'Presentation',
    'Spreadsheet',
    'WordProcessor',
    '2DGraphics',
    'VectorGraphics',
    'RasterGraphics',
    '3DGraphics',
    'Scanning',
    'OCR',
    'Photography',
    'Publishing',
    'Viewer',
    'TextTools',
    'DesktopSettings',
    'HardwareSettings',
    'Printing',
    'PackageManager',
    'Dialup',
    'InstantMessaging',
    'Chat',
    'IRCClient',
    'Feed',
    'FileTransfer',
    'HamRacio',
    'News',
    'P2P',
    'RemoteAccess',
    'Telephony',
    'TelephonyTools',
    'VideoConference',
    'WebBrowser',
    'WebDevelopment',
    'Midi',
    'Mixer',
    'Sequencer',
    'Tuner',
    'TV',
    'AudioVideoEditing',
    'Player',
    'Recorder',
    'DiscBurning',
    'ActionGame',
    'AdventureGame',
    'ArcadeGame',
    'BoardGame',
    'BlocksGame',
    'CardGame',
    'KidsGame',
    'LogicGame',
    'RolePlaying',
    'Shooter',
    'Simulation',
    'SportsGame',
    'StrategyGame',
    'Art',
    'Construction',
    'Music',
    'Languages',
    'ArtificialIntelligence',
    'Astronomy',
    'Biology',
    'Chemistry',
    'ComputerScience',
    'DataVisualization',
    'Economy',
    'Electricity',
    'Geography',
    'Geology',
    'Geoscience',
    'History',
    'Humanities',
    'ImageProcessing',
    'Literature',
    'Maps',
    'Math',
    'NumericalAnalysis',
    'MedicalSoftware',
    'Physics',
    'Robotics',
    'Spirituality',
    'Sports',
    'ParallelComputing',
    'Amusement',
    'Archiving',
    'Compression',
    'Electronics',
    'Emulator',
    'Engineering',
    'FileTools',
    'FileManager',
    'TerminalEmulator',
    'Filesystem',
    'Monitor',
    'Security',
    'Accessibility',
    'Calculator',
    'Clock',
    'TextEditor',
    'Documentation',
    'adult',
    'Core',
    'KDE',
    'GNOME',
    'XFCE',
    'GTK',
    'Qt',
    'Motif',
    'Java',
    'ConsoleOnly'
  );
  


implementation

end.
