unit main_rst;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}

interface

implementation
uses
 msei18nglob,mselanglink;
const
 resourcedata: record size: integer; data: array[0..192] of byte end =
      (size: 193; data: (
  109,97,105,110,46,114,115,95,97,98,111,117,116,0,77,83,69,103,117,105,
  32,83,104,111,114,116,99,117,116,32,67,114,101,97,116,111,114,46,13,13,
  67,114,195,169,97,116,101,117,114,32,100,101,32,114,97,99,99,111,117,114,
  99,105,115,32,112,111,117,114,32,76,105,110,117,120,46,13,13,86,111,117,
  115,32,97,105,100,101,32,195,160,32,99,114,195,169,101,114,32,117,110,32,
  114,97,99,99,111,117,114,99,105,32,118,101,114,115,32,108,39,97,112,112,
  108,105,99,97,116,105,111,110,32,100,101,32,118,111,116,114,101,32,99,104,
  111,105,120,44,32,115,117,114,32,108,101,32,98,117,114,101,97,117,32,101,
  116,32,100,97,110,115,32,108,101,32,109,101,110,117,32,100,101,115,32,97,
  112,112,108,105,99,97,116,105,111,110,115,46,0)
 );

var
 hookbefore: registerresourcehookty;

procedure registerresource(const registerresourceproc: registerresourcety);
begin
 registerresourceproc(@resourcedata);
 registerresourcehook:= hookbefore;
end;

initialization
 hookbefore:= registerresourcehook;
 registerresourcehook:= @registerresource;
end.
