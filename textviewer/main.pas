
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  msetypes, mseglob, mseguiglob, mseguiintf, mseapplication, msestat, msemenus,
  msegui, msegraphics, msegraphutils, mseevent, mseclasses, msewidgets, mseforms,
  mseimage, msebitmap, msekeyboard, mseact,
  msedataedits, msedatanodes, msedragglob, msedropdownlist, mseedit,
  msefiledialog, msegrids, msegridsglob, mseificomp, mseificompglob, mseifiglob,
  mselistbrowser, msestatfile, msestream, msesys, msesimplewidgets, msefileutils,
  msewidgetgrid, mseeditglob, mserichstring, msesyntaxedit, msetextedit,
  sysutils, classes, math,
  findfiles;

type
  tmainfo = class(tmainform)
    edDirectory: tfilenameedit;
    lbFilesCount: tlabel;
    btLeft: tbutton;
    btRight: tbutton;
    btQuit: tbutton;
    wgText: twidgetgrid;
    teText: ttextedit;
    procedure FormCreate(const sender: TObject);
    procedure FormDestroy(const sender: TObject);
    procedure FormKeyDown(const sender: twidget; var ainfo: keyeventinfoty);
    procedure FileNameEditChange(const sender: TObject);
    procedure ButtonQuitExecute(const sender: TObject);
    procedure ButtonLeftExecute(const sender: TObject);
    procedure ButtonRightExecute(const sender: TObject);
    procedure FormLoaded(const sender: TObject);
  protected
    FList: TStringList;
    FIndex: integer;
    procedure LoadText(const AFileName: string);
  end;

var
  mainfo: tmainfo;

implementation

uses
  main_mfm;

procedure tmainfo.FormCreate(const sender: TObject);
begin
  FList := TStringList.Create;
  FIndex := -1;
end;

procedure tmainfo.FormDestroy(const sender: TObject);
begin
  FList.Free;
end;

procedure tmainfo.FormKeyDown(const sender: twidget; var ainfo: keyeventinfoty);
begin
  case ainfo.key of
    KEY_LEFT: ButtonLeftExecute(sender);
    KEY_RIGHT: ButtonRightExecute(sender);
    KEY_ESCAPE: Close;
  end;
end;

procedure tmainfo.LoadText(const AFileName: string);
begin
  teText.LoadFromFile(AFileName);
  Caption := ExtractFileName(AFileName);
end;

procedure tmainfo.FileNameEditChange(const sender: TObject);
const
  CFileNameExpr = '^(.+\.c|.+\.mfm|.+\.pas|.+\.prj|.+\.sh|.+\.txt)$';
begin
  FList.Clear;
  SearchFiles(
    FList,
{$IFDEF mswindows}
    tosysfilepath(edDirectory.Value),
{$ELSE}
    edDirectory.Value,
{$ENDIF}
    CFileNameExpr,
    FALSE (* FALSE = Don't search in subdirectories. *)
    );
  lbFilesCount.Caption := Format('Found %d files in directory.', [FList.Count]);
  if FList.Count > 0 then
  begin
    FIndex := 0;
    LoadText(FList[FIndex]);
  end else
    FIndex := -1;
end;

procedure tmainfo.ButtonQuitExecute(const sender: TObject);
begin
  Close;
end;

procedure tmainfo.ButtonLeftExecute(const sender: TObject);
begin
  Dec(FIndex);
  if FIndex < 0 then
    FIndex := Pred(FList.Count);
  LoadText(FList[FIndex]);
end;

procedure tmainfo.ButtonRightExecute(const sender: TObject);
begin
  Inc(FIndex);
  if FIndex > Pred(FList.Count) then
    FIndex := 0;
  LoadText(FList[FIndex]);
end;

procedure tmainfo.FormLoaded(const sender: TObject);
begin
  edDirectory.Value := ExtractFileDir(ParamStr(0));
end;

end.
