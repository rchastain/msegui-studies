
uses
  SysUtils, Classes, FindFiles;

var
  LList: TStringList;
  
begin
  LList := TStringList.Create;
  SearchFiles(LList, '..', '^(.+\.bak|.+\.dbg|.+\.o|.+\.ppu)$', TRUE);
  WriteLn(LList.Text);
  LList.Free;
end.
